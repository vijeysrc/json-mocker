'use strict';

let jsonMocker = require('../');

let mockdata = jsonMocker.build({
  count: 25,
  index: 100,
  template: {
    _id: 'index()',
    firstName: 'firstName()',
    lastName: 'lastName()',
    bestFriend: 'sibling()',
    friends: 'siblings(5)'
  }
});

console.log(JSON.stringify(mockdata, null, 2));